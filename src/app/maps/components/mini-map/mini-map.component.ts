import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';

import { LngLatLike, Map, Marker, LngLat } from 'mapbox-gl';



@Component({
  selector: 'mini-map',
  templateUrl: './mini-map.component.html',
  styleUrl: './mini-map.component.css'
})
export class MiniMapComponent implements AfterViewInit {
  @Input() lngLat?:[number,number];
  @ViewChild('map') divMap?:ElementRef
  public map?:Map;
  public zoom:number=10;
  public currentLngLat?:LngLatLike;
  
  ngAfterViewInit(): void {
    if(!this.divMap) throw ('El elemento html no fue encontrado');
    if(!this.lngLat) throw ('Las coordenadas no fueron enviadas')
    const [lgn,lat]=this.lngLat;
    this.currentLngLat = new LngLat(lgn,lat)
    this.map = new Map({
      accessToken: 'pk.eyJ1Ijoic2FjYXJvcXVpIiwiYSI6ImNraHA2dzhqMDBhcTgydGwxa2J4MzJuOHMifQ.FxubppTsQMoEek31zAZ_Ag',
      container: this.divMap.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: this.currentLngLat, // starting position [lng, lat]
      zoom: this.zoom, // starting zoom
      interactive:false
    });

    const marker=new Marker({
      color:'red'
    }).setLngLat(this.currentLngLat).addTo(this.map)
    
  }
  

}
