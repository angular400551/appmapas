import { Component, ElementRef, ViewChild } from '@angular/core';
import { LngLatLike, Map, Marker, LngLat } from 'mapbox-gl'; 

interface markerAndColor {
  color:string,
  marker:Marker
}

interface PlainMarker{
  color:string,
  lngLat:number[]
}

@Component({
  selector: 'markers-page',
  templateUrl: './markers-page.component.html',
  styleUrl: './markers-page.component.css'
})
export class MarkersPageComponent {
  @ViewChild('map') divMap?:ElementRef
  public zoom:number=10;
  public map?:Map;
  public currentLngLat:LngLatLike=[-74.5, 40];
  public markers:markerAndColor[]=[];
  
  ngAfterViewInit(): void {
    if(!this.divMap) throw ('El elemento html no fue encontrado')
    
    this.map = new Map({
      accessToken: 'pk.eyJ1Ijoic2FjYXJvcXVpIiwiYSI6ImNraHA2dzhqMDBhcTgydGwxa2J4MzJuOHMifQ.FxubppTsQMoEek31zAZ_Ag',
      container: this.divMap.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: this.currentLngLat, // starting position [lng, lat]
      zoom: this.zoom, // starting zoom
    });

    this.readFromLocalStorage();

    // const marketHtml = document.createElement('div');
    // marketHtml.innerHTML = 'marker samir';
    
    // const marker= new Marker({
    //   // color:'red'
    //   element:marketHtml
    // }).setLngLat(this.currentLngLat).addTo(this.map);
    
  }

  createMarker(){
    if(!this.map) return
    const color= '#xxxxxx'.replace(/x/g, y=>(Math.random()*16|0).toString(16));
    const lngLat= this.map.getCenter();
    this.addMarker(lngLat,color);

  }


  addMarker(lngLat:LngLatLike, color:string){
    if(!this.map) return
    const marker=new Marker({
      color:color,
      draggable:true
    }).setLngLat(lngLat).addTo(this.map)
    
    this.markers.push({
      color:color,
      marker:marker
    })
    this.saveToLocalStorage()
    
    marker.on('dragend',(even)=>{
      this.saveToLocalStorage();
    })
  }

  deleteMarker(i:number){
    console.log('dbclick');
    this.markers[i].marker.remove();
    this.markers.splice(i,1)
  }

  toFly(marker:Marker){
    if(!this.map) return
    this.map.flyTo({
      zoom:14,
      center: marker.getLngLat()
    })
  }

  saveToLocalStorage(){
    const plainMarkers:PlainMarker[]= this.markers.map(({color, marker})=>{
      const lng=marker.getLngLat().lng;
      const lat=marker.getLngLat().lat;
      return {
        color:color,
        lngLat:[lng,lat]
      }      
    })
    
    localStorage.setItem('plainMarkers',JSON.stringify(plainMarkers));
  }

  readFromLocalStorage(){
    const plainMarkersString= localStorage.getItem('plainMarkers') ?? '[]';
    const plainMarkers:any[]= JSON.parse(plainMarkersString)
    plainMarkers.forEach(({color,lngLat}) => {
      const [lgn,lat]=lngLat;
      const coord= new LngLat(lgn,lat);      
      this.addMarker(coord,color);
    });
  }
}
