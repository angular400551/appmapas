import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { LngLatLike, Map } from 'mapbox-gl'; 

@Component({
  selector: 'zoom-range-page',
  templateUrl: './zoom-range-page.component.html',
  styleUrl: './zoom-range-page.component.css'
})
export class ZoomRangePageComponent implements AfterViewInit , OnDestroy {
  
  @ViewChild('map') divMap?:ElementRef
  public zoom:number=10;
  public map?:Map;
  public currentLngLat:LngLatLike=[-74.5, 40];
  public lng:number=-74.5;
  public lat:number=40;
  ngAfterViewInit(): void {
    if(!this.divMap) throw ('El elemento html no fue encontrado')
    
    this.map = new Map({
      accessToken: 'pk.eyJ1Ijoic2FjYXJvcXVpIiwiYSI6ImNraHA2dzhqMDBhcTgydGwxa2J4MzJuOHMifQ.FxubppTsQMoEek31zAZ_Ag',
      container: this.divMap.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: this.currentLngLat, // starting position [lng, lat]
      zoom: this.zoom, // starting zoom
    });
    this.mapListeners();
  }

  ngOnDestroy(): void {
    this.map?.remove();
  }

  mapListeners(){
    if (!this.map) throw 'Mapa no inicializado';
    this.map.on('zoom',ev=>{
      this.zoom=this.map!.getZoom()
    })

    this.map.on('zoom',ev=>{
      if(this.map!.getZoom() < 18) return
      this.map?.zoomTo(18);
    })

    this.map.on('move',ev=>{
      // console.log(this.map?.getCenter());
      this.currentLngLat=this.map!.getCenter();
      const {lng,lat}=this.map!.getCenter();
      this.lng=lng
      this.lat=lat;
    })
  }

  zoomIn(){
    this.map?.zoomIn();
  }
  
  zoomOut(){
    this.map?.zoomOut();
  }

  zoomChange(value:string){
    this.zoom=Number(value);
    this.map?.zoomTo(this.zoom);
  }
}
