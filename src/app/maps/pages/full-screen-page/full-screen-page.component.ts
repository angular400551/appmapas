import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
// (mapboxgl as any).accessToken = 'pk.eyJ1Ijoic2FjYXJvcXVpIiwiYSI6ImNraHA2dzhqMDBhcTgydGwxa2J4MzJuOHMifQ.FxubppTsQMoEek31zAZ_Ag';

@Component({
  selector: 'full-screen-page',
  templateUrl: './full-screen-page.component.html',
  styleUrl: './full-screen-page.component.css'
})
export class FullScreenPageComponent implements AfterViewInit {
 
  @ViewChild('map') divMap?:ElementRef


  ngAfterViewInit(): void {
    if(!this.divMap) throw ('El elemento html no fue encontrado')
    
    const map = new mapboxgl.Map({
      accessToken: 'pk.eyJ1Ijoic2FjYXJvcXVpIiwiYSI6ImNraHA2dzhqMDBhcTgydGwxa2J4MzJuOHMifQ.FxubppTsQMoEek31zAZ_Ag',
      container: this.divMap.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: [-74.5, 40], // starting position [lng, lat]
      zoom: 9, // starting zoom
    });
  }
}
