# MapasApp
No usar directamente en angularCLI ( a menos que ya esten creadas las variables de entorno), ya que las variables de entorno se crean basados en el .env 

## Pasos
1. Clonar el .env.template y renombrarlo a .env
2. Llenar las variables de entorno acorde
3. Crear angular envs (opcinal)
```
 npm run envs

```
4. Para development ejecutar:
```
 npm run start

```
5. Para produccion ejecutar:
```
 npm run build

```


